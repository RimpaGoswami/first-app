import React,{Component} from 'react';
import ToolbarAndroid from '@react-native-community/toolbar-android';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,
    Dimensions,SafeAreaViewComponent,AsyncStorage,Image, Alert
    } from 'react-native'
   
import {Avatar, Caption, Drawer, Title} from 'react-native-paper'
import {Card} from 'react-native-cards';
import ImagePicker from 'react-native-image-picker';
class Prof_edit extends Component{

    constructor(props){        
        super(props);  
        this.state={
        uname:'',
        first:'',
        last:'',
        age:'',
        pic:'./profpic.png',
        file:{

        }
       
        }
     }
     componentDidMount(){
        alert('Component did Mount')
        this.setProf()
     }

     

     setProf=async()=>{
      
        try {
           const userid = await AsyncStorage.getItem('emailid');
           const fn = await AsyncStorage.getItem('fname');
           const ln = await AsyncStorage.getItem('lname');
           const prof_pic=await AsyncStorage.getItem('image');
           const yr=await AsyncStorage.getItem('age')
           const ap=await AsyncStorage.getItem('api')
           console.log(ap)
           var api=JSON.parse(ap)
           console.log(api)
           console.log(fn)
           console.log(prof_pic)
           console.log(yr)
           var usri=JSON.parse(userid)
           var fname=JSON.parse(fn)
           var lname=JSON.parse(ln)
           var ages=JSON.parse(yr)
           var prof=JSON.parse(prof_pic)
          // fetchuri=uri(prof)                                                
           alert(fname + ' ' + lname + ' ' + ages)
           this.setState({uname:usri})
           this.setState({first:fname})
           this.setState({last:lname})
           this.setState({age:ages})
           this.setState({pic:prof})
        //   console.log(this.state.fname)
           //}
         } catch (error) {
           console.log(error)
         }
    }

    onUpdate=async()=>{
    
        const formdata = new FormData();
        const ap=await AsyncStorage.getItem('api')
        console.log(ap)
        var api=JSON.parse(ap)
        console.log(api)
        formdata.append('api_token',api)
        console.log(this.state.pic)
        formdata.append('profile_image', this.state.file)
        formdata.append('first_name', this.state.first);
        formdata.append('last_name', this.state.last);
        formdata.append('age',this.state.age);
        var postData={
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'somevar': 'whatever you want to pass'
                },
            body: formdata,
    }
        alert(this.state.file.uri)
        console.log(this.state.file)
        return fetch("https://cognicreate.com/MC-APP/api/update-profile", postData)
        .then((response) => response.json())
        .then((responseJson) => {

            console.log('responseJson',responseJson);
            return responseJson;

        })
        .catch((error) => {
            
            console.log('error',error);

        });
}


selectImage=async()=>{
  
    ImagePicker.showImagePicker({noData:true,mediaType:'photo'}, (response) => {
        console.log('Response = ', response);
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
    
                 this.setState({file:{
                    uri:response.uri,
                    type:response.type,
                    name:response.fileName
              
                 }});
          console.log(response)
          alert(response.fileName)
          this.setState({pic:response.uri})
        }
      });
}


     render(){
        return(
            <View style={styles.container}>
            <ToolbarAndroid style={styles.toolb}

                navIcon={require('../Assets/icons/icons_menu_24.png') }
                 onIconClicked={()=>this.props.navigation.openDrawer()}
                title="Edit Profile" titleColor='#FFF'

            >


            </ToolbarAndroid>
                            
                            <View style={styles.contOne}>
                                <Avatar.Image style={styles.avatar}
                                        
                                        source={{uri:this.state.pic}}
                                         size={200}
                            />
                            <Button 
                                 title="Select Photo" style={{flexDirection:'row'}}
                                 onPress={this.selectImage}>
                            </Button>
                            </View>
                                 <View style={styles.viewOne}>
                                 <View style={styles.viewTwo}>
                                
                            <TextInput style={styles.inputBox}
                                 underlineColorAndroid='rgba(0,0,0,0)' 
                                 defaultValue={this.state.first}
                                 onChangeText={(first) => this.setState({first:first})}
                                 value={this.state.first}
                                 placeholderTextColor = "#002f6c"
                                 selectionColor="#fff"
                        ></TextInput>
                         <TextInput style={styles.inputBox}
                                underlineColorAndroid='rgba(0,0,0,0)' 
                                defaultValue={this.state.last}
                                onChangeText={(last) => this.setState({last:last})}
                                value={this.state.last}
                                placeholderTextColor = "#002f6c"
                            
                        ></TextInput>

                       
                        
                        
                         <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                           // placeholder="Age"
                           defaultValue={this.state.age}
                            alignItems='center'
                            onChangeText={(age) => this.setState({age:age})}
                            value={this.state.age}
                            placeholderTextColor = "#002f6c"
                            
                        ></TextInput>

                         <TouchableOpacity style={styles.button}> 
                         <Text style={styles.buttonText} onPress={this.onUpdate}
                          >Save</Text>
                         </TouchableOpacity>
                         </View>
                        
                        </View >        
            </View>
        
        )
     }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems:"center",
        backgroundColor:'#c9c6c1'
        
    },
    avatar:{
        
        alignItems:'center',
        justifyContent:'center',
        width:200,
        height:200,
        borderRadius:100,
        justifyContent:'center',
        borderWidth:2
    },
    textStyle:{
        fontSize:25,
        color: '#E37F32',
        marginTop:250


    },
    toolb:{
        backgroundColor: '#2196F3',
        height: 56,
        alignSelf: 'stretch',
        textAlign: 'center',
        marginTop:23
         },

    contOne:{
        flexDirection:'column',
        marginLeft:10,
        marginTop:10

    },
    viewOne:{
        paddingTop:40,flex:1
    },
    viewTwo:{
        flexDirection:'column',
        backgroundColor:'#3F8CE8',
        flex:1
    },
    contTwo:{
        marginLeft:15,
        flexDirection:'column'
        },
    button: {
        width: 80,
        backgroundColor: '#F3DDB5',
        alignSelf:'center',
        borderRadius: 15,
        marginVertical: 10,
        paddingVertical: 12,
     
        marginTop:30,
        
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#000',
        textAlign: 'center'
    },
    inputBox: {
        width: 300,
        backgroundColor: '#C3D8F1', 
        margin:10,
        borderRadius: 10,
        paddingVertical:10,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#002f6c',
        padding:20
        
        
    }
})
export default Prof_edit;
