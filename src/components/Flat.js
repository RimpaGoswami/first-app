import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,
    AsyncStorage,FlatList,Dimensions
    } from 'react-native';
import {Card} from 'react-native-cards';



const Flat=()=>{

    const studentList=[
        {ID:'1',RollNo:'101',Name:'Robi Chandra'},
        {ID:'2',RollNo:'102',Name:'Bobi Dutta'},
        {ID:'3',RollNo:'103',Name:'Soma Das'},
        {ID:'4',RollNo:'104',Name:'Bhabesh Singh'}

    ];
     const renderItem = ({ item }) => (

     <Card style={styles.cardview}>
      <View>   
    <View style={styles.container} >
    <Text style={styles.textStyle}> RollNo: </Text>
     <Text style={styles.textStyle}>  {item.RollNo} </Text>
     
     </View>
     <View style={styles.container} >
    <Text style={styles.textStyle}> Name: </Text>
     <Text style={styles.textStyle}>  {item.Name} </Text>
     
     </View>
    </View>
    </Card>
    )
    const dimensions = Dimensions.get('window');
    const screenWidth = Dimensions.width;
      
    return(
    <View>
        <Text>I am Flat</Text>
        <TouchableOpacity> 
                    <Text>Show FlatList</Text>
        </TouchableOpacity>
                
    <FlatList style={{width: screenWidth}}
    data={studentList}
    renderItem={renderItem}
    keyExtractor={(item)=>item.ID}
    />


        
    </View>    
    )
}

const styles = StyleSheet.create({container: {
    flexDirection:'row',
    alignSelf: 'center'
},
 textStyle:{
    flex:1,
    fontSize:15,

},
viewStyle:{
    
    flexDirection:'row',
    marginLeft:20,
    padding:10

},
cardview:{
    margin:5,
    borderRadius:10,
    backgroundColor:'pink'
}


});

export default Flat;



