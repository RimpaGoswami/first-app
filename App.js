
import 'react-native-gesture-handler';
import React,{Component} from 'react';
import {DrawerRouter, NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import { createDrawerNavigator } from '@react-navigation/drawer';

//import { createDrawerNavigator } from 'react-navigation-drawer';

import { StyleSheet, 
  Text,
  View,
  TouchableOpacity,
  AsyncStorage
  } from 'react-native';
import Login from "./src/screens/Login"
import Signup from "./src/screens/Signup"
import Dashboard from "./src/screens/Dashboard"
import Prof_edit from "./src/screens/Prof_edit"
import Sidebar from "./src/components/Sidebar"
import Emoji from "./src/screens/Emoji"
const Stack=createStackNavigator()

const profStack=createStackNavigator()

const emojiStack=createStackNavigator()

const profStackScreen=({navigation})=>(
  <profStack.Navigator headerMode='none'>

    <profStack.Screen name='Prof_edit' component={Prof_edit}/>
  
  </profStack.Navigator>
)

const emojiStackScreen=({navigation})=>(
  <emojiStack.Navigator headerMode='none'>

    <emojiStack.Screen name='Emoji' component={Emoji}/>
  
  </emojiStack.Navigator>
)
  
const Drawer=createDrawerNavigator();


function DrawerRoute() {
  return (
  

    <Drawer.Navigator drawerContent={props=> <Sidebar {...props}  />  }  >
    
        
        <Drawer.Screen  name="Dashboard" component={Dashboard}   />
        <Drawer.Screen name="Prof_edit"   component={profStackScreen}/>
        <Drawer.Screen name="Emoji" component={emojiStackScreen}/>
       
      </Drawer.Navigator>
  )
}


class App extends Component
  {
    render(){
  return(

    


     <NavigationContainer>
    
       <Stack.Navigator  headerMode='none'>
       <Stack.Screen name="Login" component={Login}
          options={{ title: 'Welcome' } }
        />
      <Stack.Screen name="Signup" component={Signup} />
      <Stack.Screen name="Dashboard" component={DrawerRoute}/>
      </Stack.Navigator>
  
  </NavigationContainer>

  )
  }
}


export default App

