import React, { Component } from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,AsyncStorage
    } from 'react-native';

     const Signup=({navigation})=> {

    
            return(
                <View style={styles.container}>
                    <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
                    <Text style={styles.textStyle}>Registration </Text>
                   <View style={styles.name}> 
                    <TextInput style={styles.inputBox}
                   // onChangeText={(email) => this.setState({email})}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    placeholder="First Name"
                    
            
                    placeholderTextColor = "#002f6c"
                    selectionColor="#fff"
                   // keyboardType="email-address"
                ></TextInput>
                    
                    <TextInput style={styles.inputBox}
                   // onChangeText={(password) => this.setState({password})} 
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    placeholder="Last Name"
                   // secureTextEntry={true}
                    placeholderTextColor = "#002f6c"
                    //ref={(input) =>password = input}
                    />
                    </View>
                    <View>
                    <TextInput style={styles.inputBoxSecond}
                   // onChangeText={(email) => this.setState({email})}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    placeholder="Email Address"
                    
            
                    placeholderTextColor = "#002f6c"
                    selectionColor="#fff"
                   // keyboardType="email-address"
                ></TextInput> 

                <TextInput style={styles.inputBoxThird}
                   // onChangeText={(email) => this.setState({email})}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    placeholder="Create Password"
                    secureTextEntry={true}

                    
            
                    placeholderTextColor = "#002f6c"
                    selectionColor="#fff"
                   // keyboardType="email-address"
                ></TextInput> 
                        
                    </View> 
                    
                    


                     <TouchableOpacity style={styles.button}> 
                    <Text style={styles.buttonText}>Signup</Text>
                </TouchableOpacity>
         

                    <View style={styles.signupTextContent}>
                        <Text style={styles.signupText}>Already have an account?</Text>
                         <TouchableOpacity>
                             <Text style={{color:'#002f6c'}} onPress={()=>navigation.navigate('Login')}>Signin</Text></TouchableOpacity>
                    </View>
     
    
                </View>
                
            );
            }
        


    

    const styles = StyleSheet.create({
        container: {
            flex:1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor:'#615f5c'
        },
        textStyle:{
            fontSize:25,
            color: '#E37F32'


        },
        inputBox: {
            width: 175,
            backgroundColor: '#c9c6c1', 
            
            borderRadius:15,
            borderColor:'#fff',
            paddingVertical:10,
            paddingHorizontal: 16,
            fontSize: 16,
            color: '#002f6c',
            marginVertical: 10,
            marginLeft:20,
            alignItems:"center"
        },

        inputBoxSecond: {
            width: 300,
            backgroundColor: '#c9c6c1', 
            alignItems:'center',
           // borderRadius:15,
            borderColor:'#fff',
            paddingVertical:10,
            paddingHorizontal: 16,
            fontSize: 16,
            color: '#002f6c',
            marginVertical: 10,
            marginLeft:20,
            alignItems:"center"
        },

        inputBoxThird: {
            width: 250,
            backgroundColor: '#c9c6c1', 
            alignItems:'center',
            alignSelf:'center',
            borderRadius:15,
            borderColor:'#fff',
            paddingVertical:10,
            paddingHorizontal: 16,
            fontSize: 16,
            color: '#002f6c',
            marginVertical: 10,
            marginLeft:20,
            alignItems:"center"
        },
        button: {
            width: 150,
            backgroundColor: '#F3DDB5',
            borderRadius: 25,
            marginVertical: 10,
            paddingVertical: 12,
            alignSelf:'center'
        },
        buttonText: {
            fontSize: 16,
            fontWeight: '500',
            color: '#4076c2',
            textAlign: 'center'
        },
        signupTextContent:{
         
            
            alignItems:'center',
            justifyContent:'flex-end',
            marginVertical:16,
            flexDirection:'row'
            
        },
        signupText:{
            color:'#E37F32',
            fontSize:16
        },
        name:{
            alignItems:'center',
            marginVertical:16,
            flexDirection:'row'

        }
    });

    export default Signup;