import { NavigationContainer } from '@react-navigation/native';
import React,{Component} from 'react';
import Flat  from "../components/Flat"
import ToolbarAndroid from '@react-native-community/toolbar-android';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity, AsyncStorage
    } from 'react-native';
  

    class Dashboard extends Component{

        constructor(props){        
            super(props);        
           
        }
        onPressLogout=async () =>{

          var log=await AsyncStorage.getItem('loggedIn');
          console.log(log)
          if(log==='true'){
          AsyncStorage.removeItem('emailid');
          AsyncStorage.removeItem('lname');
          AsyncStorage.removeItem('image');

          
         this.props.navigation.navigate('Login');
         alert('You have been logged out.');
        }
     }


        render(){
          return(
         <View>
        <ToolbarAndroid style={styles.toolb}
          navIcon={require('../Assets/icons/icons_menu_24.png') }
         onIconClicked={()=>this.props.navigation.openDrawer()}
           title="Dashboard"  
         actions={[{title: 'LogOut', show: 'always'}]}
         onActionSelected={this.onPressLogout}>
        </ToolbarAndroid>
            <View>
            <Flat/>
            </View>
           </View> 

          );
        
          }
  }
  const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
  
    },
    toolb:{
        backgroundColor: '#2196F3',
        height: 56,
        alignSelf: 'stretch',
        textAlign: 'center',
        marginTop:23
         }
  })
  export default Dashboard;
    
    

