import React,{Component} from 'react';

import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,
    Dimensions,SafeAreaViewComponent,AsyncStorage
    } from 'react-native'
   
import {Avatar, Caption, Drawer, Title} from 'react-native-paper'
import {DrawerItem,DrawerContentScrollView,DrawerItemList} from '@react-navigation/drawer'
import { SafeAreaProvider } from 'react-native-safe-area-context';


import { NativeViewGestureHandler } from 'react-native-gesture-handler';

class Sidebar extends Component{

    constructor(...props){        
        super(...props);  
        this.state={
        uname:'Email',
        name:'User Name',
        pic:' '
       
    }
}


componentDidMount(){
        alert('Component did Mount')
        this.checking()

}

     checking=async()=>{
      
         try {
            const userid = await AsyncStorage.getItem('emailid');
            const usernm = await AsyncStorage.getItem('name');
            const prof_pic=await AsyncStorage.getItem('image');
            const ap=await AsyncStorage.getItem('api')
            console.log(ap)
            var api=JSON.parse(ap)
            console.log(api)
            console.log(usernm)
            console.log(prof_pic)
            var usri=JSON.parse(userid)
            var usrn=JSON.parse(usernm)
            var prof=JSON.parse(prof_pic)
           // fetchuri=uri(prof)
                                                                 
            alert(usri + '\n' + usrn)
            this.setState({uname:usri})
            
            this.setState({name:usrn})
            this.setState({pic:prof})
            console.log(this.state.pic)

              //alert(this.state.uname);
            //}
          } catch (error) {
            console.log(error)
          }
     }
    render(){
        return(


             <SafeAreaProvider>
                <View style={{flex:1}}>
                       <DrawerContentScrollView {...this.props}>
                   
                        <View>
                            <View style={styles.contOne}>
                                <Avatar.Image style={styles.avatar}

                                        source={{uri:this.state.pic}}
                            />
                            <View style={styles.contTwo}>
                                
                                <Title>{this.state.name}</Title>
                                    <Caption style={{fontSize:15}}>
                                        {this.state.uname}
                                    </Caption>
                                    <TouchableOpacity style={styles.button}>
                                    <Text style={styles.buttonText} onPress={() => this.props.navigation.navigate('Prof_edit')}>Edit Profile

                                    </Text>

                                    </TouchableOpacity>
                            </View>
                            </View>
                        </View>
                       <Drawer.Section>
                            
                            <DrawerItem style={{backgroundColor:'#96E6F0'}}
                            
                             label="Dashboard"
                             onPress={() => this.props.navigation.navigate('Dashboard')}
                                  />
                            

                        </Drawer.Section>   
                        <Drawer.Section>
                            
                            <DrawerItem style={{backgroundColor:'#96E6F0'}}
                            
                             label="Emoji"
                             onPress={() => this.props.navigation.navigate('Emoji')}
                                  />
                            

                        </Drawer.Section>   

                </DrawerContentScrollView>
                </View>  
                </SafeAreaProvider>
                       
        )
    }


}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    contOne:{
        flexDirection:'row',
        marginLeft:10,
        marginTop:10

    },
    contTwo:{
        marginLeft:15,
        flexDirection:'column'
        },
button: {
    width: 150,
    backgroundColor: '#F7A753',
    borderRadius: 15,
    marginVertical: 10,
    paddingVertical: 12
},
avatar:{
        
 
    width:60,
    height:60,
    borderRadius:30,
    color:'red',
    overflow:"hidden",
    borderColor:"blue",
    borderWidth:2
},
buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#4076c2',
    textAlign: 'center'
}
})
export default Sidebar;