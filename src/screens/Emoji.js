import React, { Component } from "react";
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,
    AsyncStorage,FlatList,Dimensions
    } from 'react-native';
import {Card} from 'react-native-cards';
import {Icon } from 'react-native-vector-icons/MaterialCommunityIcons'
import {Avatar, Caption, Drawer, Title} from 'react-native-paper'
import ToolbarAndroid from '@react-native-community/toolbar-android';


class Emoji extends Component{
    constructor(props){        
        super(props);  
        this.state={ 
              emojiList:[
                {ID:'1',src:'https://www.pikpng.com/pngl/b/520-5200525_download-smiley-face-no-background-clipart-smiley-emoticon.png'},
                {ID:'2',src:'https://wi.wallpapertip.com/wsimgs/175-1754906_sadness-smiley-emoticon-clip-art-holy-family-catholic.jpg'},
                {ID:'3',src:'https://www.wallpapertip.com/wmimgs/175-1754842_emoticon-smiley-face-with-tears-of-joy-emoji.png'},
                {ID:'4',src:'https://www.itl.cat/pngfile/big/309-3092744_face-with-tears-of-joy-emoji-sticker-emoticon.png'},
                {ID:'5',src:'https://www.wallpapertip.com/wmimgs/175-1756222_free-png-download-smiley-face-happy-png-images.png'},
                {ID:'6',src:'https://comps.canstockphoto.com/half-smile-emoticon-clip-art-vector_csp23300266.jpg'},
                {ID:'7',src:'https://www.wallpapertip.com/wmimgs/175-1754842_emoticon-smiley-face-with-tears-of-joy-emoji.png'},
                {ID:'8',src:'https://www.wallpapertip.com/wmimgs/175-1754842_emoticon-smiley-face-with-tears-of-joy-emoji.png'},
                {ID:'9',src:'https://www.itl.cat/pngfile/big/309-3092744_face-with-tears-of-joy-emoji-sticker-emoticon.png'},
                {ID:'10',src:'https://www.pikpng.com/pngl/b/520-5200525_download-smiley-face-no-background-clipart-smiley-emoticon.png'},
                {ID:'11',src:'https://wi.wallpapertip.com/wsimgs/175-1754906_sadness-smiley-emoticon-clip-art-holy-family-catholic.jpg'},
                {ID:'12',src:'https://www.wallpapertip.com/wmimgs/175-1754842_emoticon-smiley-face-with-tears-of-joy-emoji.png'},
                {ID:'13',src:'https://www.wallpapertip.com/wmimgs/175-1756222_free-png-download-smiley-face-happy-png-images.png'},
                {ID:'14',src:'https://comps.canstockphoto.com/half-smile-emoticon-clip-art-vector_csp23300266.jpg'},
                {ID:'15',src:'https://www.wallpapertip.com/wmimgs/175-1754842_emoticon-smiley-face-with-tears-of-joy-emoji.png'},
                {ID:'16',src:'https://www.wallpapertip.com/wmimgs/175-1754842_emoticon-smiley-face-with-tears-of-joy-emoji.png'},
        
            ]

        };
        dimensions = Dimensions.get('window');
        screenWidth = Dimensions.width;

        }

    

       
      renderItem = ({ item }) => (
    <View style={{padding:10}}>

     
    <Card style={styles.cardview}>
    <View style={styles.container}>
    
     

    <Avatar.Image source={{uri:item.src}}


        />
        </View>
       </Card>
       </View>
     
   

    
    )
    
   render(){
    return(
    <View style={styles.mainView}>
       <ToolbarAndroid style={styles.toolb}

navIcon={require('../Assets/icons/icons_menu_24.png') }
 onIconClicked={()=>this.props.navigation.openDrawer()}
title="Feelings" titleColor='#FFF' textAlign='center'

/>
     <View>
    <FlatList style={styles.flat} numColumns={4}
    data={this.state.emojiList}
    renderItem={this.renderItem}
    keyExtractor={(item)=>item.ID}
    />

   </View> 
        
    </View>    
    )
}
}
const styles = StyleSheet.create({container: {
   
    backgroundColor:'#F3DF9E'
   
},
 textStyle:{
    
    
    fontSize:15,
    alignItems:'center',
    alignSelf:'center'

},
mainView:{
    flex:1, 
    flexDirection:'column',
    backgroundColor:'#F3DF9E'

},
flat:{
    flexDirection:'column',
    paddingTop:20,
    alignSelf:'center',
    width:Dimensions.width

},
toolb:{
    backgroundColor: '#2196F3',
    height: 56,
    alignSelf: 'stretch',
    textAlign: 'center',
    marginTop:23,
    
     },
avatar:{
    width:50,
    height:50,
    borderRadius:25,
    color:'red',
    overflow:"hidden",
    
   
},
cardview:{
    
    width:50,
    height:50,
    borderRadius:25,
    backgroundColor:'#F3DF9E'
}


});
export default Emoji;